'use strict';
/* global Offer */
/**
 * OfferController
 *
 * @description :: Server-side logic for managing offers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	createNew: function(req, res){
		var options = {
			shipper:  req.session.user.id,
			status:   'new',
			delivery: req.param('deliveryId'),
			sum:      req.param('theOffer')
		};
		Offer.create(options).exec(function(err, createdOffer) {
			if (err) {
				// if(err.invalidAttributes && err.invalidAttributes.email &&
				// 	err.invalidAttributes.email[0] && err.invalidAttributes.email[0].rule === 'unique'){
				// 	return res.send(409, 'כתובת הדואר האלקטרוני הזו כבר במערכת, אנא נסה שוב.');
				// }
				console.log('err creating offer', err);
				return res.negotiate(err);
			}
			return res.json({
				createdOffer: createdOffer
			});
		});
	}

};


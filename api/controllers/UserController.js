/* global User */
'use strict';
var Emailaddresses = require('machinepack-emailaddresses');
var Passwords = require('machinepack-passwords');
module.exports = {
	signup: function(req, res){
		if (_.isUndefined(req.param('email'))) {
      return res.badRequest('חובה להזין כתובת דואר אלקטרוני!');
    }
    if (_.isUndefined(req.param('password'))) {
      return res.badRequest('חובה להזין סיסמה!');
    }
    if (req.param('password').length < 6) {
      return res.badRequest('אורך מינימום של סיסמה 6 תווים!');
    }
    if (_.isUndefined(req.param('firstname'))) {
      return res.badRequest('חובה להזין שם פרטי!');
    }
		if (_.isUndefined(req.param('lastname'))) {
      return res.badRequest('חובה להזין שם משפחה!');
    }
		Emailaddresses.validate({
      string: req.param('email')
    }).exec({
      error: function(err) {
        return res.serverError(err);
      },
      invalid: function() {
        return res.badRequest('אנא בדוק  את כתובת הדואר האלקטרוני!');
      },
      success: function() {
				Passwords.encryptPassword({
          password: req.param('password')
        }).exec({
          error: function(err) {
            return res.serverError(err);
          },
          success: function(result) {
						var options = {
							usertype:    req.param('usertype'),
							firstname:   req.param('firstname'),
							lastname:    req.param('lastname'),
							companyname: req.param('companyname'),
							email:       req.param('email'),
							phone:       req.param('phone'),
              passwordencrypted: result
            };
						User.create(options).exec(function(err, createdUser) {
              if (err) {
								if(err.invalidAttributes && err.invalidAttributes.email &&
									err.invalidAttributes.email[0] && err.invalidAttributes.email[0].rule === 'unique'){
									return res.send(409, 'כתובת הדואר האלקטרוני הזו כבר במערכת, אנא נסה שוב.');
							 }
                return res.negotiate(err);
              }
							req.session.userId = createdUser.id;
              return res.json(createdUser);
            });
					}
        });
      },
    });
	},
	login: function(req, res){
		User.findOne({
			email: req.param('email')
		}).exec(function(err, user){
			if(err){ return res.negotiate(err); }
			if(!user){ return res.notFound(); }
			Passwords.checkPassword({
        passwordAttempt: req.param('password'),
        encryptedPassword: user.passwordencrypted
      }).exec({
        error: function (err){ return res.negotiate(err); },
        incorrect: function (){ return res.notFound(); },
        success: function (){
					delete user.passwordencrypted;
					req.session.user = user;
					// console.log(req.session.user);
					return res.ok(user);
				}
      });
		});
	},
	logout: function(req, res){
		if (!req.session.userId){
			return res.redirect('/');
		}
		User.findOne(req.session.userId, function foundUser(err, user) {
      if (err){ return res.negotiate(err); }
      if (!user) {
        console.log.verbose('Session refers to a user who no longer exists.');
        return res.redirect('/');
      }
      req.session.userId = null;
      return res.redirect('/');
    });
	}
};

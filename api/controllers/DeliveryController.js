/* global Delivery, Package, Base, User, Offer */
'use strict';/**
 * DeliveryController
 *
 * @description :: Server-side logic for managing deliveries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	createNew: function(req, res){
		// console.log('allParams::::', req.allParams());
		// return res.json(req.allParams());
		var numofpackages;
		if(req.param('packages').length===1){
			numofpackages = req.param('packages')[0].amount;
		}else{
			numofpackages = _.reduce(req.param('packages'), function(partialsum, b){
				return partialsum + b.amount;
			}, 0);
		}
		var options = {
			client:             req.session.user.id,
			status:             'new',
			fromaddresscity:        req.param('fromaddresscity'),
			fromaddressstreet:      req.param('fromaddressstreet'),
			fromaddresshousenumber: req.param('fromaddresshousenumber'),
			fromaddresscompany:     req.param('fromaddresscompany'),
			fromaddressarea:        req.param('fromaddressarea'),
			toaddresscity:          req.param('toaddresscity'),
			toaddressstreet:        req.param('toaddressstreet'),
			toaddresshousenumber:   req.param('toaddresshousenumber'),
			toaddresscompany:       req.param('toaddresscompany'),
			toaddressarea:          req.param('toaddressarea'),
			fromdatetime:       req.param('fromdatetime'),
			todatetime:         req.param('todatetime'),
			shippmentloading:   req.param('shippmentloading') || 'manual',
			shippmentunloading: req.param('shippmentunloading') || 'manual',
			lastofferdatetime:  req.param('lastofferdatetime'),
			comments:           req.param('comments'),
			numofpackages:      numofpackages
		};
		Delivery.create(options).exec(function(err, createdDelivery) {
			if (err) {
				// if(err.invalidAttributes && err.invalidAttributes.email &&
				// 	err.invalidAttributes.email[0] && err.invalidAttributes.email[0].rule === 'unique'){
				// 	return res.send(409, 'כתובת הדואר האלקטרוני הזו כבר במערכת, אנא נסה שוב.');
				// }
				return res.negotiate(err);
			}
			var packages = [];
			_.forEach(req.param('packages'), function(pack){
				pack.delivery = createdDelivery.id;
				packages.push(pack);
			});
			Package.create(packages).exec(function(err, createdPackages){
				if(err){
					console.log('err creating packages', err);
					return res.negotiate(err);
				}
				return res.json({
					createdDelivery: createdDelivery,
					createdPackages: createdPackages
				});
			});
		});
	},
	myOrders: function(req, res){
		var options = {
			client: req.session.user.id,
			status: ['new', 'shipperselected']
		};
		Delivery.find(options).sort('id DESC').populate('offers', { limit: 3, sort: 'sum ASC' })
		.exec(function(err, myDeliveries) {
			if (err) {
				return res.negotiate(err);
			}
			var filteredDeliveries = [];
			var currentDtMinusHour = new Date((new Date()).getTime() - 1000*60*60);
			_.forEach(myDeliveries, function(o){
				if(!!o.lastofferdatetime && new Date(o.lastofferdatetime) > currentDtMinusHour){
					o.offers = [];
				}
				filteredDeliveries.push(o);
			});
			return res.json(filteredDeliveries);
		});
	},
	myOrdershistory: function(req, res){
		// show deliveries where lastoffer is in past.
		var options = {
			or : [
				{ client: req.session.user.id },
				{ shipper: req.session.user.id }
			],
			datetimefinaloffer: { '<=': new Date() }
			// status: ['done', 'cancelled']
		};
		if(req.param('filterBy')){
			// filterBy=done or filterBy=any other string==>(not done)
			if(req.param('filterBy')==='done'){
				options.status = 'done';
			}else{
				options.status = {'!': 'done'};
			}
		}
		Delivery.find(options).populate('offers', { limit: 3, sort: 'sum ASC'}).sort('id DESC').exec(function(err, myDeliveries) {
			if (err) {
				return res.negotiate(err);
			}
			return res.json(myDeliveries);
		});
	},
	getNewDelivery: function(req, res){
		var options = {
			id: req.param('id')
		};
		Delivery.findOne(options).populate('offers', { limit: 3, sort: 'sum ASC'}).populate('packages').exec(function(err, oneDelivery) {
			if (err) {
				return res.negotiate(err);
			}
			Base.find().exec(function(err, bases){
				if (err) {
					return res.negotiate(err);
				}
				var allBases = {};
				_.forEach(bases, function(o){
					allBases[o.id] = o;
				});
				User.find().exec(function(err, users){
					if (err) {
						return res.negotiate(err);
					}
					var allUsers = {};
					_.forEach(users, function(o){
						allUsers[o.id] = o;
					});
					return res.json({
						allBases: allBases,
						allUsers: allUsers,
						oneDelivery: oneDelivery
					});
				});
			});
		});
	},
	getNewDeliveries: function(req, res){
		var options = {};
		if(req.param('onlyMineAsShipper')){
			options.shipper = req.session.user.id;// filter only *MINE*
			console.log('added onlyMineAsShipper');
		}
		// console.log('filterBy::', req.param('filterBy'));
		if(req.param('filterBy')){
			var filterBy = JSON.parse(req.param('filterBy'));
			// console.log(typeof filterBy, filterBy);
			// { age: { '<': 30 }}
			// { status: 'new' }
			if( filterBy.field === 'date' ){
				var toT = new Date((new Date()).getTime()+24*60*60*1000*filterBy.days),
				fromT = new Date();
				fromT.setHours(0);
				fromT.setMinutes(0);
				fromT.setSeconds(0);
				toT.setHours(23);
				toT.setMinutes(59);
				toT.setSeconds(59);
				if(filterBy.afterhour){
					fromT.setHours(filterBy.afterhour);
					console.log('after hour!', fromT);
				}
				options.fromdatetime = {
					'>': fromT,
					'<=': toT
				};
				if(filterBy.days===10000){
					options.fromdatetime['>'] = new Date((new Date()).getTime()-24*60*60*1000*filterBy.days);
				}
			}else if( filterBy.field === 'packages' ){
				options.numofpackages = {
					'<=': filterBy.maxnumofpackages
				};
			}
		}
		// Delivery.find(options).sort('id DESC').exec(function(err, myDeliveries) {
		Delivery.find(options).populate('packages').populate('offers', { limit: 3, sort: 'sum ASC'}).sort('id DESC').exec(function(err, myDeliveries) {
			//console.log('myDeliveries', myDeliveries);
			if (err) {
				return res.negotiate(err);
			}
			return res.json(myDeliveries);
		});
	},
	chooseWinner: function(req, res){
		console.log(req.param('deliveryId'), req.param('offerId'));
		Offer.findOne(req.param('offerId')).exec(function(err, offer){
			console.log('OFFER:::', offer.shipper);
			Delivery.update(req.param('deliveryId'), {
				status: 'shipperselected',
				shipper: offer.shipper
			}, function afterDeliveryUpdateStatus(err, updatedD){
				if (err) { return res.negotiate(err); }
				Offer.update(req.param('offerId'), {
					status: 'winning'
				}, function afterOfferStatusUpdate(err, updatedO){
					if (err) { return res.negotiate(err); }
					return res.json({
						updatedDelivery: updatedD,
						updatedOffer: updatedO
					});
				});
			});

		});
	},
	approveByShipper: function(req, res){
		console.log(req.param('deliveryId'), req.param('offerId'));
		Delivery.update(req.param('deliveryId'), {status: 'shipperapproved'}, function afterDeliveryUpdateStatus(err, updatedD){
			if (err) { return res.negotiate(err); }
			Offer.update(req.param('offerId'), {status: 'shipperapproved'}, function afterOfferStatusUpdate(err, updatedO){
				if (err) { return res.negotiate(err); }
				return res.json({
					updatedDelivery: updatedD,
					updatedOffer: updatedO
				});
			});
		});
	}
};

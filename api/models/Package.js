/**
 * Package.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    height: {
      type: 'integer'
    },
    weight: {
      type: 'float'
    },
    amount: {
      type: 'integer'
    },
    image: {
      type: 'longtext'
    },
    base:{
      model: 'base'
    },
    delivery: {
      model: 'delivery'
    }
  }
};


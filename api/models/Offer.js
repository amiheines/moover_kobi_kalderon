/**
 * Offer.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    sum: {
      type: 'float'
    },
    status: {
      type: 'string',
      enum: ['new', 'winning', 'shipperapproved', 'cancelled']
    },
    shipper: {
      model: 'user'
    },
    delivery: {
      model: 'delivery'
    }
  }
};


/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
'use strict';
module.exports = {
  attributes: {
    admin: {
      type: 'boolean'
    },
    usertype: {
      type: 'string',
      enum: ['client', 'shipper']
    },
    firstname: {
      type: 'string'
    },
    lastname: {
      type: 'string'
    },
    companyname: {
      type: 'string'
    },
    email: {
      type: 'email',
      email: 'true',
      unique: 'true'
    },
    phone: {
      type: 'string'
    },
    passwordencrypted: {
      type: 'string'
    },
    carmodel: {
      type: 'string'
    },
    licenseplate: {
      type: 'string'
    },
    image: {
      type: 'string'
    },
    rating: {
      type: 'float'
    },
    deliveries: {
      collection: 'delivery',
      via: 'client'
    },
    offers: {
      collection: 'offer',
      via: 'shipper'
    },
    toJSON: function(){
      var modelAttributes = this.toObject();
      delete modelAttributes.password;
      delete modelAttributes.passwordencrypted;
      return modelAttributes;
    }
  }
};


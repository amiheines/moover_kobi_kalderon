'use strict';
/**
 * Delivery.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    fromaddresscity: { type: 'string' },
    fromaddressstreet: { type: 'string' },
    fromaddresshousenumber: { type: 'string' },
    fromaddresscompany: { type: 'string' },
    fromaddressarea: {
      type: 'string',
      enum: [
        'שפלה',
        'ב"ש והסביבה',
        'ירושלים',
        'גוש דן',
        'חיפה והקריות',
        '',
      ]
    },
    toaddresscity: { type: 'string' },
    toaddressstreet: { type: 'string' },
    toaddresshousenumber: { type: 'string' },
    toaddresscompany: { type: 'string' },
    toaddressarea: {
      type: 'string',
      enum: [
        'שפלה',
        'ב"ש והסביבה',
        'ירושלים',
        'גוש דן',
        'חיפה והקריות',
        '',
      ]
    },
    fromdatetime: {
      type: 'datetime'
    },
    todatetime: {
      type: 'datetime'
    },
    loadontype: {
      type: 'string',
      enum: ['forklift', 'manual', 'ramp']
    },
    loadofftype: {
      type: 'string',
      enum: ['forklift', 'manual', 'ramp']
    },
    client: {
      model: 'user'
    },
    shipper: {
      model: 'user'
    },
    status: {
      type: 'string',
      enum: ['new', 'shipperselected', 'shipperapproved', 'done', 'cancelled']
    },
    offers: {
      collection: 'offer',
      via: 'delivery'
    },
    packages: {
      collection: 'package',
      via: 'delivery'
    },
    numofpackages: {
      type: 'integer'
    },
    datetimefinaloffer: {
      type: 'datetime'
    },
    comments: {
      type: 'string'
    },
    // toJSON: function() {
    //   var obj = this.toObject();
    //   if(!!this.fromaddress){
    //     obj.fullFromAddress = this.fromaddress;
    //   }else{
    //     obj.fullFromAddress = this.fromaddresscity + ', ' + this.fromaddressstreet + ' ' + this.fromaddresshousenumber;
    //   }
    //   if(!!this.toaddress){
    //     obj.fullToAddress = this.toaddress;
    //   }else{
    //     obj.fullToAddress = this.toaddresscity + ', ' + this.toaddressstreet + ' ' + this.toaddresshousenumber;
    //   }
    //   return obj;
    // }
  }
};


'use strict';
angular.module('mooverClientApp')
.controller('AdminDeliveryCtrl', function ($scope, $http, $stateParams) {
  $scope.populatePage = function(){
    toast();
    $http({
      url: '/delivery/'+$stateParams.id,
      method: 'GET'
    })
    .then(function successGettingBase(res){
      $scope.value = res.data;
    }, function errorGettingBase(err){
      console.log('error in getting myOrders', err);
    });
  };
  $scope.populatePage();
});

'use strict';
angular.module('mooverClientApp')
.controller('AdminPackagesCtrl', function ($scope, $http, $uibModal) {
  $scope.populatePage = function(){
    toast();
    $http({
      url: '/package?sort=id DESC',
      method: 'GET'
    })
    .then(function successGettingBase(res){
      $scope.items = res.data.results;
    }, function errorGettingBase(err){
      console.log('error in getting myOrders', err);
    });
  };
  $scope.populatePage();

  $scope.delete = function(id){
    $scope.deleteID = id;
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'modalDeleteConfirm.html',
      controller: 'ModalDeleteConfirmInstanceCtrl',
      resolve: {
        title:      function () { return 'מחיקת חבילה';      },
        recordtype: function () { return 'חבילה ' + id; },
      }
    });
    modalInstance.result.then(function(){
      toast();
      $http({
        url: '/package/' + $scope.deleteID,
        method: 'DELETE'
      })
      .then(function successGettingBase(){
        $scope.populatePage();
      }, function errorGettingBase(err){
        console.log('error in delete.', err);
      });
    }, function () {
      // console.log('Modal dismissed at: ' + new Date());
    });
  };
});

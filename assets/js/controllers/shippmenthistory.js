'use strict';
angular.module('mooverClientApp')
.controller('ShippmentHistoryCtrl', function ($scope, $http, $location) {
  $http.get('/delivery/myordershistory')
  .then(function successGettingBase(res){
    $scope.myorders = res.data;
  }, function errorGettingBase(err){
    console.log('error in getting myOrders', err);
  });

  $scope.viewOffers = function(deliveryId){
    // console.log('view offers!', deliveryId);
    $location.path('/offersForDelivery/' + deliveryId);
  };
  $scope.$parent.filterShippmentHistory = function(filterby){
    var params = {
      url: '/delivery/myordershistory',
      method: 'GET'
    };
    if(filterby!=='all'){ params.params = { filterBy: filterby }; }
    toast();
    $http(params)
    .then(function successGettingBase(res){
      $scope.myorders = res.data;
    }, function errorGettingBase(err){
      console.log('error in getting myOrders', err);
    });
  };
});

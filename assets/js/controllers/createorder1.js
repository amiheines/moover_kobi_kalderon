'use strict';
angular.module('mooverClientApp')
  .controller('CreateOrder1Ctrl',
  ['$scope', '$http', 'toastr', '$rootScope', 'passDataBetweenPages',
  function($scope, $http, toastr, $rootScope, passDataBetweenPages){
  $scope.dateOptions = {
    formatYear: 'yyyy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
  $scope.open1 = function() { $scope.popup1.opened = true; };
  $scope.popup1 = { opened: false };
  $scope.open2 = function() { $scope.popup2.opened = true; };
  $scope.popup2 = { opened: false };
  $scope.datepickerformat = 'dd/MM/yyyy';
  var d = new Date();
  d.setHours(14);
  d.setMinutes(0);
  $scope.fromtime = $scope.totime = d;

  // prepare the baseTypes Data
  $http.get('/base')
  .then(function successGettingBase(res){
    passDataBetweenPages.setItem('basetypes', res.data.results);
  }, function errorGettingBase(res){
    console.log('failed getting base info', res);
  });

  $scope.submitSignupForm = function(){
    $scope.signupForm.fromdatetime.setHours(  $scope.fromtime.getHours());
    $scope.signupForm.fromdatetime.setMinutes($scope.fromtime.getMinutes());
    $scope.signupForm.todatetime.setHours(  $scope.totime.getHours());
    $scope.signupForm.todatetime.setMinutes($scope.totime.getMinutes());
    passDataBetweenPages.setItem('formData', {
      fromaddress:  $scope.signupForm.fromaddress,
      fromdatetime: $scope.signupForm.fromdatetime,
      toaddress:    $scope.signupForm.toaddress,
      todatetime:   $scope.signupForm.todatetime
    });
    window.location = '#/createorder2';
  };
}]);

'use strict';
angular.module('mooverClientApp')
.controller('ModalDeleteConfirmCtrl', function () {
})
.controller('ModalDeleteConfirmInstanceCtrl', function ($scope, $uibModalInstance, title, recordtype) {
  $scope.title = title;
  $scope.recordtype = recordtype;
  $scope.ok = function () {
    $uibModalInstance.close();
  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
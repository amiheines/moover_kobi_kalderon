/*global $*/
'use strict';
angular.module('mooverClientApp')
.controller('EmptyCtrl', function ($scope) {

  // $('.container-fluid-innermooverdarker').css('background-color', 'transparent');

  $scope.detectmob = function() {
    if( navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/BlackBerry/i) ||
      navigator.userAgent.match(/Windows Phone/i)
    ){
      return true;
    }else{
      return false;
    }
  };
  if($scope.detectmob() && window.location.hash==='#/empty'){
    window.location = '#/mainmenu';
  }

  $scope.$on("$destroy", function(){
    $('.container-fluid-innermooverdarker').css('background-color', 'rgba(0,0,0,.7)');
  });

});

/*global toast*/
'use strict';
angular.module('mooverClientApp')
.controller('OfferCtrl', function ($scope, $http, $stateParams, $uibModal, toastr, $window, $rootScope, $interval) {

  // console.log('param ID::', $stateParams.id);
  $interval(function(){
    if((new Date($scope.newdelivery.lastofferdatetime)).getTime() < (new Date()).getTime()){
      console.log('too late TIMER...');
      $scope.newdelivery.tooLate = true;
    }
  }, 1000);
  $scope.populatePage = function(){
    toast();
    $http({
      url: '/delivery/getNewDelivery',
      method: 'GET',
      params: { id: $stateParams.id }
    })
    .then(function successGettingBase(res){
      $scope.newdelivery = res.data.oneDelivery;
      // console.log('newdelivery.lastofferdatetime', (new Date($scope.newdelivery.lastofferdatetime)).getTime(), (new Date()).getTime());
      if((new Date($scope.newdelivery.lastofferdatetime)).getTime() < (new Date()).getTime()){
        console.log('too late...');
        $scope.newdelivery.tooLate = true;
      }
      $scope.allBases = res.data.allBases;
      $scope.ImTheChosenShipper = false;
      $scope.wonOffer = _.find($scope.newdelivery.offers, function(o){
        console.log(o);
        return o.status==='winning';
      });
      if($scope.wonOffer && $rootScope.currentUser.id === $scope.wonOffer.shipper){
        $scope.ImTheChosenShipper = true;
      }
    }, function errorGettingBase(err){
      console.log('error in getting myDelivery', err);
    });
  };
  $scope.populatePage();
  $scope.backpage = function(){
    $window.history.back();
  };

  $scope.approveShippment = function(){
    console.log('approve shippment by shipper', $scope.newdelivery.id, $scope.wonOffer.id);
    $http.post('/delivery/approveByShipper', {
      deliveryId: $scope.newdelivery.id,
      offerId: $scope.wonOffer.id
    });
  };

  $scope.submitOffer = function(id, $event){
    $event.stopPropagation();
    $scope.ID = id;
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'modalInput.html',
      controller: 'ModalInputInstanceCtrl',
      resolve: {
        title: function () { return 'מתן הצעה';          },
        body:  function () { return 'הצעה להזמנה ' + id; }
      }
    });
    modalInstance.result.then(function(theOffer){
      console.log('the offer:', theOffer, $scope.ID);
      $http.post('/offer/createNew', {
        deliveryId: $scope.ID,
        theOffer: theOffer
      })
      .then(function onSuccess(/*sailsResponse*/){
        toastr.success('הפרטים התקבלו, ההזמנה בטיפול.');
        window.location = '#/mainmenu';
      })
      .catch(function onError(sailsResponse){
        console.log('error in submit offer.', sailsResponse);
      });
    }, function () {
      // console.log('Modal dismissed at: ' + new Date());
    });
  };

});

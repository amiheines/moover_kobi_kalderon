'use strict';
angular.module('mooverClientApp')
.controller('OrderStatusCtrl', function($scope, $http, $location) {
  $http.get('/delivery/myorders')
  .then(function successGettingBase(res){
    $scope.myorders = res.data;
  }, function errorGettingBase(err){
    console.log('error in getting myOrders', err);
  });

  $scope.viewOffers = function(deliveryId){
    // console.log('view offers!', deliveryId);
    $location.path('/offersForDelivery/' + deliveryId);
  };
});

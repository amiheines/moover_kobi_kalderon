'use strict';
angular.module('mooverClientApp')
.controller('ModalInputInstanceCtrl', function ($scope, $uibModalInstance, title, body) {
  $scope.title = title;
  $scope.body = body;
  $scope.ok = function () {
    $uibModalInstance.close($scope.modalInputData);
  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
'use strict';
angular.module('mooverClientApp')
  .controller('CreateOrder2Ctrl',
  ['$scope', '$http', 'toastr', '$rootScope', 'passDataBetweenPages',
  function($scope, $http, toastr, $rootScope, passDataBetweenPages){
    $scope.basetypes = passDataBetweenPages.getItem('basetypes');
    $scope.packages = [{
        height: 40,
        weight: 1,
        amount: 1,
        base: undefined
    }];
    $scope.addPackage = function(){
      $scope.packages.push({
        height: 40,
        weight: 1,
        amount: 1,
        base: undefined
      });
    };
    // $scope.uploadPackageImage = function(id){
    //   console.log('open modal upload file for ', id);
    //   var modalInstance = $uibModal.open({
    //     animation: true,
    //     templateUrl: 'modalInput.html',
    //     controller: 'ModalInputInstanceCtrl',
    //     resolve: {
    //       title: function () { return 'העלה תמונה';          },
    //       body:  function () { return 'תמונה של חבילה ' + id; }
    //     }
    //   });
    //   modalInstance.result.then(function(theOffer){
    //     console.log('the offer:', theOffer, $scope.ID);
    //     $http.post('/offer/createNew', {
    //       deliveryId: $scope.ID,
    //       theOffer: theOffer
    //     })
    //     .then(function onSuccess(/*sailsResponse*/){
    //       toastr.success('הפרטים התקבלו, ההזמנה בטיפול.');
    //       window.location = '#/mainmenu';
    //     })
    //     .catch(function onError(sailsResponse){
    //       console.log('error in submit offer.', sailsResponse);
    //     });
    //   }, function () {
    //     // console.log('Modal dismissed at: ' + new Date());
    //   });
    //
    // };
    //
    // $scope.uploadPackageImage = function(myForm){
    //   console.log('form submitted, upload package image file', myForm, $scope.value.packageimagefile);
    //   $http.post('/packageimage/uploadImage', $scope.value.packageimagefile)
    //   .then(function onSuccess(sailsResponse){
    //     console.log('packageimage/uploadImage TODO: hook up the image to the delivery!', sailsResponse);
    //   })
    //   .catch(function onError(sailsResponse){
    //     console.log(sailsResponse);
    //     toastr.error(sailsResponse.data);
    //     return;
    //   })
    //   .finally(function eitherWay(){
    //   });
    // };
    $scope.getTotalBases = function(){
      var total = 0;
      for(var i = 0; i < $scope.packages.length; i++){
        total += $scope.packages[i].amount;
      }
      return total;
    };
    $scope.submitSignupForm = function(){
      passDataBetweenPages.setItem('packages', $scope.packages);
      window.location = '#/createorder3';
    };
}]);

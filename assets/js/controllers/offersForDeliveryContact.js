'use strict';
/*global $*/
angular.module('mooverClientApp')
.controller('OffersForDeliveryContactCtrl', function ($scope, $http, $stateParams, $uibModal, toastr, $window) {
  $scope.populatePage = function(){
    toast();
    $http({
      url: '/delivery/getNewDelivery',
      method: 'GET',
      params: { id: $stateParams.deliveryId }
    })
    .then(function successGettingBase(res){
      $scope.newdelivery = res.data.oneDelivery;
      $scope.allBases = res.data.allBases;
      $scope.allUsers = res.data.allUsers;
      // $scope.selectedUser = res.data.oneDelivery.[]
      $scope.offer = _.find(res.data.oneDelivery.offers, function(o){
        return o.id==$stateParams.offerId;
      });
      $scope.shipper = _.find($scope.allUsers, function(o){ return o.id==$scope.offer.shipper; });
    }, function errorGettingBase(err){
      console.log('error in getting myDelivery', err);
    });
  };
  $scope.populatePage();

  $scope.backpage = function($event){
    if($($event.target).is('a')){
      return;
    }
    $window.history.back();
  };

  $scope.approveShipper = function(){
    $http.post('/delivery/chooseWinner', {
      deliveryId: $stateParams.deliveryId,
      offerId: $stateParams.offerId
    })
    .then(function onSuccess(/*sailsResponse*/){
      toastr.success('בחרת חברת הובלה, חסכת 240 ש"ח בהזמנה זו.');
      window.location = '#/mainmenu';
    })
    .catch(function onError(sailsResponse){
      console.log('error in submit offer.', sailsResponse);
    });
  };
});

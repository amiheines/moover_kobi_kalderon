'use strict';
angular.module('mooverClientApp')
.controller('PlannedShippmentsCtrl', function ($scope, $http, $uibModal, $location) {

  $scope.filterBy = {};

  $scope.viewOffer = function(offerID){
    console.log('--->>/offer/' + offerID);
    $location.path('/offer/' + offerID);
  };

  $scope.populatePage = function(){
    toast();
    $http({
      url: '/delivery/getNewDeliveries',
      method: 'GET',
      params: { filterBy: $scope.filterBy, onlyMineAsShipper: true }
    })
    .then(function successGettingBase(res){
      $scope.newdeliveries = res.data;
    }, function errorGettingBase(err){
      console.log('error in getting myOrders', err);
    });
  };
  $scope.populatePage();

  $scope.$parent.filterByArea = function(){
    $scope.modalOptions = {
      title: 'סנן לפי אזור',
      items: [
        {
          field: 'area',
          area: 1,
          display: 'שפלה'
        },
        {
          field: 'area',
          area: 2,
          display: 'ב"ש והסביבה'
        },
        {
          field: 'area',
          area: 3,
          display: 'ירושלים'
        },
        {
          field: 'area',
          area: 4,
          display: 'גוש דן'
        },
        {
          field: 'area',
          area: 5,
          display: 'חיפה והקריות'
        },
        {
          field: 'area',
          area: -1,
          display: 'הכל'
        }
      ]
    };
    $scope.openModalFilter();
  };
  $scope.$parent.filterByDate = function(){
    $scope.modalOptions = {
      title: 'סנן לפי תאריך',
      items: [
        {
          field: 'date',
          days: 1,
          display: 'היום'
        },
        {
          field: 'date',
          days: 7,
          display: 'השבוע הקרוב'
        },
        {
          field: 'date',
          days: 31,
          display: 'החודש הקרוב'
        },
        {
          field: 'date',
          days: 10000,
          display: 'הכל'
        }
      ]
    };
    $scope.openModalFilter();
  };
  $scope.$parent.filterByPackages = function(){
    $scope.modalOptions = {
      title: 'סנן לפי מספר חבילות',
      items: [
        {
          field: 'packages',
          maxnumofpackages: 1,
          display: '1'
        },
        {
          field: 'packages',
          maxnumofpackages: 2,
          display: '1-2'
        },
        {
          field: 'packages',
          maxnumofpackages: 3,
          display: '1-3'
        },
        {
          field: 'packages',
          maxnumofpackages: 10000,
          display: 'הכל'
        }
      ]
    };
    $scope.openModalFilter();
  };
  $scope.animationsEnabled = true;
  $scope.openModalFilter = function () {
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      resolve: {
        options: function () {
          return $scope.modalOptions;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
      // console.log('Modal dismissed at: ' + new Date(), $scope.selected);
      $('.navbar-toggle').click();//  close menu in mobile
      $scope.filterBy = selectedItem;
      $scope.populatePage();
    });
  };


})
.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, options) {
  $scope.title = options.title;
  $scope.items = options.items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $uibModalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

});

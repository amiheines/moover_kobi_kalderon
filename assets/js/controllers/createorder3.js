'use strict';
angular.module('mooverClientApp')
  .controller('CreateOrder3Ctrl',
  ['$scope', '$http', 'toastr', '$rootScope', 'passDataBetweenPages',
  function($scope, $http, toastr, $rootScope, passDataBetweenPages){
  var orderData = passDataBetweenPages.getAll();
  $scope.dateOptions = {
    formatYear: 'yyyy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
  $scope.open1 = function() { $scope.popup1.opened = true; };
  $scope.popup1 = { opened: false };
  $scope.datepickerformat = 'dd/MM/yyyy';
  var d = new Date();
  d.setHours(14);
  d.setMinutes(0);
  $scope.lastoffertime = d;

  $scope.signupForm = {
    loading: false
  };
  $scope.submitSignupForm = function(){
    $scope.signupForm.loading = true;

    $scope.signupForm.lastofferdatetime.setHours(  $scope.lastoffertime.getHours());
    $scope.signupForm.lastofferdatetime.setMinutes($scope.lastoffertime.getMinutes());

    var newOrder = {
      fromaddress: orderData.formData.fromaddress,
      fromdatetime: orderData.formData.fromdatetime,
      toaddress: orderData.formData.toaddress,
      todatetime: orderData.formData.todatetime,
      shippmentloading: $scope.signupForm.shippmentLoading,
      shippmentunloading: $scope.signupForm.shippmentUnloading,
      lastofferdatetime: $scope.signupForm.lastofferdatetime,
      comments: $scope.signupForm.comments,
      packages: orderData.packages
    };
    $http.post('/delivery/createNew', newOrder)
    .then(function onSuccess(/*sailsResponse*/){
      toastr.success('הפרטים התקבלו, ההזמנה בטיפול.');
      window.location = '#/mainmenu';
      // console.log('delivery/createNew', sailsResponse);
    })
    .catch(function onError(sailsResponse){
      console.log(sailsResponse.data.invalidAttributes);
      // Handle known error type(s).
      if (sailsResponse.status === 409) {
        toastr.error(sailsResponse.data);
        // $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status);
        return;
      }
      if (sailsResponse.data.invalidAttributes) {
        $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (JSON.stringify(sailsResponse.data.invalidAttributes));
        return;
      }
      // Handle unknown error type(s).
      $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (JSON.stringify(sailsResponse.data) || sailsResponse.status);
    })
    .finally(function eitherWay(){
      $scope.signupForm.loading = false;
    });
  };
}]);

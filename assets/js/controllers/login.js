'use strict';
angular.module('mooverClientApp')
  .controller('LoginCtrl',
  ['$scope', '$http', 'toastr', '$rootScope',
  function ($scope, $http, toastr, $rootScope) {
    $scope.signupForm = {
      loading: false
    };
    $scope.submitSignupForm = function(){
      $scope.signupForm.loading = true;
      $http.post('/user/signup', {
        usertype:    $scope.signupForm.usertype,
        email:       $scope.signupForm.email,
        firstname:   $scope.signupForm.firstname,
        lastname:    $scope.signupForm.lastname,
        companyname: $scope.signupForm.companyname,
        password:    $scope.signupForm.password
      })
      .then(function onSuccess(/* sailsResponse */){
        // console.log('/#/profile/' + sailsResponse.data.id);
        // window.location = '#/profile/' + sailsResponse.data.id;
        window.location = '#/mainmenu';
      })
      .catch(function onError(sailsResponse){
        console.log(sailsResponse.data.invalidAttributes);
        // Handle known error type(s).
        if (sailsResponse.status === 409) {
          toastr.error(sailsResponse.data);
          $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status);
          return;
        }
        if (sailsResponse.data.invalidAttributes) {
          $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (JSON.stringify(sailsResponse.data.invalidAttributes));
          return;
        }
        // Handle unknown error type(s).
        $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (JSON.stringify(sailsResponse.data) || sailsResponse.status);
      })
      .finally(function eitherWay(){
        $scope.signupForm.loading = false;
      });
    };
  }]);

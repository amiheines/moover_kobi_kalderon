'use strict';
// http://brewhouse.io/blog/2014/12/09/authentication-made-simple-in-single-page-angularjs-applications.html
angular.module('mooverClientApp')
  .controller('LoginModalCtrl',
  ['$scope', '$http', 'toastr', '$rootScope',
  function ($scope, $http/*, toastr, $rootScope*/) {
    this.cancel = $scope.$dismiss;
    this.submit = function (email, password) {
      $http.post('/user/login', {
        email:    email,
        password: password
      }).then(function(user){
        $scope.$close(user);
      });
    };
  }]);

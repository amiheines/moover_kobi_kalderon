'use strict';
angular.module('mooverClientApp')
  .controller('LogoutCtrl',
  // ['$scope', '$http', 'toastr', '$rootScope', 'location',
  function ($scope, $http, toastr, $rootScope, $location) {
    $http.get('/user/logout')
    .then(function onSuccess(){
    })
    .finally(function eitherWay(){
      localStorage.removeItem('mooverUser');
      $location.path('loginOrSignon');
    });
  // }]);
  });

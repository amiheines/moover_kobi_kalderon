'use strict';
/* global $ */
angular.module('mooverClientApp')
  .controller('OuterCtrl', function ($scope, $rootScope) {
    $scope.$on('$locationChangeStart', function(event, next) {
      var url = next.split('#');
      if(url.length>1){
        $scope.shipperListOfPotentialDeliveries = (url[url.length-1]==='/offers');
        $scope.showFilterHistory = (url[url.length-1].substr(0,'/shippmenthistory'.length)==='/shippmenthistory');
      }
      if($rootScope.currentUser){
        $scope.adminMenu = $rootScope.currentUser.admin;
      }else{
        $scope.adminMenu = false;
      }
    });
    $scope.collapsemenu = function(){
      $('.navbar-toggle').click();
    };
  });
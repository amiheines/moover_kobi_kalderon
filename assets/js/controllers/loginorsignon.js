'use strict';
angular.module('mooverClientApp')
  .controller('LoginorsignonCtrl', function ($rootScope, $location) {
    var cachedUser = localStorage.getItem('mooverUser');
    if(cachedUser){
      $rootScope.currentUser = JSON.parse(cachedUser);
    }else{
      $rootScope.currentUser = null;
    }
    if($rootScope.currentUser){
      $location.path('empty');
    }
  });

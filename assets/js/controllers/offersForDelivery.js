'use strict';
angular.module('mooverClientApp')
.controller('OffersForDeliveryCtrl', function ($scope, $http, $stateParams, $uibModal, toastr, $window, $state) {
  $scope.isCollapsed = true;
  // console.log('param ID::', $stateParams.id);
  $scope.populatePage = function(){
    toast();
    $http({
      url: '/delivery/getNewDelivery',
      method: 'GET',
      params: { id: $stateParams.id }
    })
    .then(function successGettingBase(res){
      $scope.newdelivery = res.data.oneDelivery;
      $scope.allBases = res.data.allBases;
      $scope.allUsers = res.data.allUsers;
      $scope.someoneWon = false;
      console.log("someoneWon?");
      for(var i=0; i<res.data.oneDelivery.offers.length; i++){
        if(res.data.oneDelivery.offers[i].status === 'winning'){
          $scope.someoneWon = true;
          console.log("someoneWon!!");
        }
      }
      console.log(res.data.oneDelivery);
    }, function errorGettingBase(err){
      console.log('error in getting myDelivery', err);
    });
  };
  $scope.populatePage();

  $scope.backpage = function(){
    $window.history.back();
  };
  $scope.toggleCollapse = function($event){
    $scope.isCollapsed = !$scope.isCollapsed;
    $event.stopPropagation();
  };

  $scope.chooseWinner = function(offerId){
    window.location = '#/offersForDeliveryContact/'+$stateParams.id+'/'+offerId;
  };

  $scope.refreshPage = function(){
    $state.reload();
  };
  // $scope.chooseWinner = function(offerId){
  //   console.log(offerId);
  //   $http.post('/delivery/chooseWinner', {
  //     deliveryId: $stateParams.id,
  //     offerId: offerId
  //   })
  //   .then(function onSuccess(/*sailsResponse*/){
  //     toastr.success('בחרת חברת הובלה, הזוכה יעודכן.');
  //     window.location = '#/mainmenu';
  //   })
  //   .catch(function onError(sailsResponse){
  //     console.log('error in submit offer.', sailsResponse);
  //   });
  // };
  // $scope.submitOffer = function(id){
  //   $scope.ID = id;
  //   var modalInstance = $uibModal.open({
  //     animation: true,
  //     templateUrl: 'modalInput.html',
  //     controller: 'ModalInputInstanceCtrl',
  //     resolve: {
  //       title: function () { return 'מתן הצעה';          },
  //       body:  function () { return 'הצעה להזמנה ' + id; }
  //     }
  //   });
  //   modalInstance.result.then(function(theOffer){
  //     console.log('the offer:', theOffer, $scope.ID);
  //     $http.post('/offer/createNew', {
  //       deliveryId: $scope.ID,
  //       theOffer: theOffer
  //     })
  //     .then(function onSuccess(/*sailsResponse*/){
  //       toastr.success('הפרטים התקבלו, ההזמנה בטיפול.');
  //       window.location = '#/mainmenu';
  //     })
  //     .catch(function onError(sailsResponse){
  //       console.log('error in submit offer.', sailsResponse);
  //     });
  //   }, function () {
  //     // console.log('Modal dismissed at: ' + new Date());
  //   });
  // };

});

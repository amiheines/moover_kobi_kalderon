'use strict';
/* global $, angular */
angular.module('mooverClientApp')
  .controller('CreateOrderCtrl',
  ['$scope', '$http', 'toastr',
  function($scope, $http, toastr){
  $scope.dateOptionsFrom = {
    formatYear: 'yyyy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
  $scope.dateOptionsTo = {
    formatYear: 'yyyy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
  $scope.dateOptionsLastOffer = {
    formatYear: 'yyyy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
  $scope.fromDatePickerChange = function(){
    $scope.dateOptionsTo.minDate        = $scope.signupForm.fromdatetime;
    $scope.dateOptionsLastOffer.maxDate = $scope.signupForm.fromdatetime;
  };
  $scope.files = [];
  $scope.step1 = true;
  $scope.step2 = false;
  $scope.step3 = false;
  $scope.open1 = function() { $scope.popup1.opened = true; };
  $scope.popup1 = { opened: false };
  $scope.open2 = function() { $scope.popup2.opened = true; };
  $scope.popup2 = { opened: false };
  $scope.datepickerformat = 'dd/MM/yyyy';
  var d = new Date();
  d.setHours(14);
  d.setMinutes(0);
  $scope.fromtime = $scope.totime = d;

  // prepare the baseTypes Data
  $http.get('/base')
  .then(function successGettingBase(res){
    // passDataBetweenPages.setItem('basetypes', res.data.results);
    $scope.basetypes = res.data.results;
  }, function errorGettingBase(res){
    console.log('failed getting base info', res);
  });
  $scope.arealist = [
    { id: 'שפלה',        description: 'שפלה' },
    { id: 'ב"ש והסביבה', description: 'ב"ש והסביבה' },
    { id: 'ירושלים',     description: 'ירושלים' },
    { id: 'גוש דן',      description: 'גוש דן' },
    { id: 'חיפה והקריות',description: 'חיפה והקריות' },
    { id: '',            description: '' },
  ];
  $scope.step1to2 = function(){
    toastr.clear();
    if(/^[0-9]* ?[א-ת]?$/.exec($scope.signupForm.fromaddresshousenumber)===null ||
       /^[0-9]* ?[א-ת]?$/.exec($scope.signupForm.toaddresshousenumber)===null){
      toastr.error('מספר בית חייב להכיל רק תווי מספר ואות אופציונלית בעברית');
      return;
    }
    if(!$scope.signupForm.fromdatetime || !$scope.signupForm.todatetime ||
      !$scope.signupForm.fromaddresscity || !$scope.signupForm.toaddresscity ||
      !$scope.signupForm.fromaddresshousenumber || !$scope.signupForm.toaddresshousenumber ||
      !$scope.signupForm.fromaddressstreet || !$scope.signupForm.toaddressstreet
     ){
      toastr.error('חובה למלא כתובת ותאריך לאיסוף ולפריקה');
      return;
    }
    $scope.signupForm.fromdatetime.setHours(  $scope.fromtime.getHours());
    $scope.signupForm.fromdatetime.setMinutes($scope.fromtime.getMinutes());
    // console.log( $scope.fromtime.getHours(), $scope.signupForm.fromdatetime.getTime());
    $scope.signupForm.todatetime.setHours(  $scope.totime.getHours());
    $scope.signupForm.todatetime.setMinutes($scope.totime.getMinutes());
    // console.log('todate time:::', $scope.signupForm.todatetime);
    if($scope.signupForm.todatetime < $scope.signupForm.fromdatetime){
      toastr.error('זמן פריקה חייב להיות אחרי זמן איסוף');
      return;
    }
    // passDataBetweenPages.setItem('formData', {
    //   fromaddress:  $scope.signupForm.fromaddress,
    //   fromdatetime: $scope.signupForm.fromdatetime,
    //   toaddress:    $scope.signupForm.toaddress,
    //   todatetime:   $scope.signupForm.todatetime
    // });
    // window.location = '#/createorder2';
    $scope.step1 = false;
    $scope.step2 = true;
  };


  $scope.packages = [{
      height: undefined,
      weight: undefined,
      amount: undefined,
      base: undefined
  }];
  $scope.addPackage = function(){
    $scope.packages.push({
      height: undefined,
      weight: undefined,
      amount: undefined,
      base: undefined
    });
  };
  $scope.deletePackage = function(){
    if($scope.packages.length>1){
      $scope.packages.pop();
    }
  };

  $scope.getTotalBases = function(){
    var total = 0;
    for(var i = 0; i < $scope.packages.length; i++){
      total += $scope.packages[i].amount?$scope.packages[i].amount:0;
    }
    return total;
  };
  $scope.flagFirstTimeStep3 = true;
  $scope.step2to3 = function(){
    toastr.clear();
    for(var i=0; i<$scope.packages.length; i++){
      if(!Number.isInteger(Number($scope.packages[i].amount)) ||
         !Number.isInteger(Number($scope.packages[i].height))){
        toastr.error('גובה וכמות צריכים להיות שלמים');
        return;
      }
    }
    for(var i=0; i<$scope.packages.length; i++){
      if(!Number.isInteger(2*Number($scope.packages[i].weight))){
        toastr.error('משקל יכול להיות בדיוק של עד חצי קילו');
        return;
      }
    }
    if(!$scope.packages[0].amount || !$scope.packages[0].height || !$scope.packages[0].weight || !$scope.packages[0].base){
      toastr.error('חובה למלא את סוג המשטח, גובה, משקל וכמות');
      return;
    }
    $scope.step2 = false;
    $scope.step3 = true;
    // console.log($scope.flagFirstTimeStep3, $scope.signupForm.fromdatetime.getTime());
    if($scope.flagFirstTimeStep3){
      $scope.flagFirstTimeStep3 = false;
      $scope.signupForm.lastofferdatetime = new Date(  $scope.signupForm.fromdatetime.getTime()-1000*60*60 );
      var tt = new Date();
      tt.setHours(  $scope.signupForm.fromdatetime.getHours() );
      tt.setMinutes(  $scope.signupForm.fromdatetime.getMinutes() );
      tt.setTime(tt.getTime()-1000*60*60);
      $scope.signupForm.lastoffertime = tt;
    }
  };
  $scope.step3to2 = function(){
    $scope.step3 = false;
    $scope.step2 = true;
  };
  $scope.step2to1 = function(){
    $scope.step2 = false;
    $scope.step1 = true;
  };

  // $scope.open1 = function() { $scope.popup1.opened = true; };
  // $scope.popup1 = { opened: false };
  // $scope.datepickerformat = 'dd/MM/yyyy';
  // var d = new Date();
  d.setHours(14);
  d.setMinutes(0);
  $scope.signupForm = {
    lastofferdatetime: new Date(),
    lastoffertime: d,
    loading: false
  };
  // http://stackoverflow.com/questions/16631702/file-pick-with-angular-js
  $scope.file_changed = function(element) {
    $scope.$apply(function(/* scope */) {
    var photofile = element.files[0];
    // console.log('photofile and element', photofile, element.attributes['data-index'].value);
    var reader = new FileReader();
    reader.onloadend = function(e) {
      // console.log('onloadEND!', e);
      var idx = $(element).data().index;
      $('.uploadimagepackage[data-index="' + idx + '"]').attr({
        src: '/images/attachment.png',
        height: '33px'
      });
      // $scope.files[element.attributes['data-index'].value] = this.result;//photofile.name;
      $scope.packages[element.attributes['data-index'].value].image = this.result;//photofile.name;
    };
    reader.readAsDataURL(photofile);
   });
};
  $scope.submitSignupForm = function(){
    $scope.signupForm.loading = true;

    $scope.signupForm.lastofferdatetime.setHours(  $scope.signupForm.lastoffertime.getHours());
    $scope.signupForm.lastofferdatetime.setMinutes($scope.signupForm.lastoffertime.getMinutes());

    var newOrder = {
      fromaddresscity: $scope.signupForm.fromaddresscity,
      fromaddressstreet: $scope.signupForm.fromaddressstreet,
      fromaddresshousenumber: $scope.signupForm.fromaddresshousenumber,
      fromaddresscompany: $scope.signupForm.fromaddresscompany,
      fromaddressarea: $scope.signupForm.fromaddressarea,
      fromdatetime: $scope.signupForm.fromdatetime,
      toaddresscity: $scope.signupForm.toaddresscity,
      toaddressstreet: $scope.signupForm.toaddressstreet,
      toaddresshousenumber: $scope.signupForm.toaddresshousenumber,
      toaddresscompany: $scope.signupForm.toaddresscompany,
      toaddressarea: $scope.signupForm.toaddressarea,
      todatetime: $scope.signupForm.todatetime,
      shippmentloading: $scope.signupForm.shippmentLoading,
      shippmentunloading: $scope.signupForm.shippmentUnloading,
      lastofferdatetime: $scope.signupForm.lastofferdatetime,
      comments: $scope.signupForm.comments,
      packages: $scope.packages
    };
    // http://shazwazza.com/post/uploading-files-and-json-data-in-the-same-request-with-angular-js/
    // toast();
    $http({
      method: 'POST',
      url: '/delivery/createNew',
      // headers: { 'Content-Type': undefined },
      // transformRequest: function (data) {
      //   var formData = new FormData();
      //   //need to convert our json object to a string version of json otherwise
      //   // the browser will do a 'toString()' on the object which will result
      //   // in the value '[Object object]' on the server.
      //   formData.append("model", angular.toJson(newOrder));
      //   //now add all of the assigned files
      //   for (var i = 0; i < data.files.length; i++) {
      //     //add each file to the form data and iteratively name them
      //     if($scope.files[i]){
      //       formData.append("file" + i, $scope.files[i]);
      //     }
      //   }
      //   return formData;
      // },
      //Create an object that contains the model and files which will be transformed
      // in the above transformRequest method
      // data: { newOrder: newOrder, files: $scope.files }
      data: newOrder
    })
    .then(function onSuccess(/*sailsResponse*/){
      toastr.clear();
      toastr.success('הפרטים התקבלו, ההזמנה בטיפול.');
      window.location = '#/mainmenu';
      // console.log('delivery/createNew', sailsResponse);
    })
    .catch(function onError(sailsResponse){
      console.log(sailsResponse.data.invalidAttributes);
      // Handle known error type(s).
      if (sailsResponse.status === 409) {
        toastr.clear();
        toastr.error(sailsResponse.data);
        // $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (sailsResponse.data || sailsResponse.status);
        return;
      }
      if (sailsResponse.data.invalidAttributes) {
        $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (JSON.stringify(sailsResponse.data.invalidAttributes));
        return;
      }
      // Handle unknown error type(s).
      $scope.signupForm.errorMsg = 'An unexpected error occurred: ' + (JSON.stringify(sailsResponse.data) || sailsResponse.status);
    })
    .finally(function eitherWay(){
      $scope.signupForm.loading = false;
    });
  };


}]);

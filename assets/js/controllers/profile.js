'use strict';

/**
 * @ngdoc function
 * @name mooverClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mooverClientApp
 */
angular.module('mooverClientApp')
  .controller('ProfileCtrl', function ($stateParams) {
    console.log('stateParams', $stateParams);
  });

'use strict';
angular.module('mooverClientApp')
  .directive('numbersOnly', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        angular.element(element).on("keypress", function(e) {
          //ignore all characters that are not numbers, except backspace, delete, left arrow and right arrow
          if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 8 && event.keyCode != 46 && event.keyCode != 37 && event.keyCode != 39) {
            event.preventDefault();
          }
        });
      }
    };
  });

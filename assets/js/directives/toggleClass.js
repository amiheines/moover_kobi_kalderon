'use strict';
angular.module('mooverClientApp')
.directive('toggleClass', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        element.toggleClass(attrs.toggleClass);
      });
    }
  };
});
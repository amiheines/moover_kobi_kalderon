'use strict';
// https://codepen.io/garethdweaver/pen/eNpWBb#anon-login
angular.module('mooverClientApp')
.directive('countdowntimer',['countdowntimerutil', '$interval', function(countdowntimerutil, $interval) {
  return {
    restrict: 'A',
    scope: { targetdate: '@' },
    link: function(scope, element) {
      scope.$watch('targetdate', function(){
        // http://stackoverflow.com/questions/20068526/angularjs-directive-does-not-update-on-scope-variable-changes
        if(!scope.targetdate){
          return;
        }
        var future;
        future = new Date(scope.targetdate);
        $interval(function () {
          var diff;
          diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
          return element.text(countdowntimerutil.dhms(diff));
        }, 1000);
      });
    }
  };
}])
.factory('countdowntimerutil', [function () {
  return {
    dhms: function (t) {
      var days, hours, minutes, seconds;
      days = Math.floor(t / 86400);
      t -= days * 86400;
      hours = Math.floor(t / 3600) % 24;
      t -= hours * 3600;
      minutes = Math.floor(t / 60) % 60;
      t -= minutes * 60;
      seconds = t % 60;
      if(days===0){
        return [
          hours + ':',
          minutes + ':',
          seconds + ''
        ].join('');
      }else{
        return [
          days + ' ימים, ',
          hours + ':',
          minutes + ':',
          seconds + ''
        ].join('');
      }
    }
  };
}]);
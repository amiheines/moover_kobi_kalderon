'use strict';

/**
 * @ngdoc directive
 * @name assetsApp.directive:disallowKeyboard
 * @description
 * # disallowKeyboard
 */
angular.module('mooverClientApp')
  .directive('disallowKeyboard', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        angular.element(element).on("keypress", function(e) {
          e.preventDefault();
        });
      }
    };
  });

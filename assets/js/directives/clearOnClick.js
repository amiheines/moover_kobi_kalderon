'use strict';
/* global $ */
angular.module('mooverClientApp')
.directive('clearOnClick', [function () {
  return {
    restrict: 'A',
    link: function (scope, element) {
      element.on('click', function () {
        $(this).val("");
      });
    }
  };
}]);
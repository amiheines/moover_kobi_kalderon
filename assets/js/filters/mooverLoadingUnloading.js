'use strict';
angular.module('mooverClientApp')
.filter('mooverLoadingUnloading', [function () {
  return function(input){
    input = input || '';
    switch (input) {
      case 'manual':
        return 'ידני';
      case 'forklift':
        return 'מלגזה';
      default:
        return 'רמפה';
    }
  };
}]);
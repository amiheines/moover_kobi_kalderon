'use strict';
/* global moment */
angular.module('mooverClientApp')
.filter('momentdate', [function () {
  return function(input){
    input = input || '';
    moment.locale('he');
    return moment(input).format('llll');
  };
}]);
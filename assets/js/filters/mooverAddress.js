'use strict';
angular.module('mooverClientApp')
.filter('mooverAddress', [function () {
  return function(input, toOrFromPrefix){
    if(!input){
      console.log('empty input?', input, ';');
      return;
    }
    if(input[toOrFromPrefix+'addresscity']){
      return input[toOrFromPrefix+'addresscity'] + ', ' +
        input[toOrFromPrefix+'addressstreet'] + ' ' +
        input[toOrFromPrefix+'addresshousenumber'];
    }else{
      return input[toOrFromPrefix+'address'];
    }
  };
}]);
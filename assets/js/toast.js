/*global $*/
(function() {
  "use strict";
  window.toast = function(text, duration) {
    text = text || 'טוען....';
    if (typeof duration !== "number" || duration <= 0) { duration = 1500; }
    var div = $('<div id="snackbar"></div>');
    $(div).html(text);
    $('body').append(div);
    $(div).addClass('show');
    setTimeout(function(){
      $(div).removeClass('show');
      $(div).remove();
    }, duration);
  };
})();

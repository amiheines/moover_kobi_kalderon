'use strict';
// http://stackoverflow.com/questions/22408790/angularjs-passing-data-between-pages
angular.module('mooverClientApp')
  .service('passDataBetweenPages', function(){
  var savedData = {};
  return {
   setItem: function(itemname, data){
     savedData[itemname] = data;
   },
   getItem: function(itemname){
     return savedData[itemname];
   },
   getAll: function(){
     return savedData;
   }
 };
});

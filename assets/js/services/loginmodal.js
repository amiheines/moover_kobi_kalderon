'use strict';
// http://brewhouse.io/blog/2014/12/09/authentication-made-simple-in-single-page-angularjs-applications.html
angular.module('mooverClientApp')
  .service('loginModal', function($uibModal, $rootScope, toastr){
    function assignCurrentUser(res){
      delete res.config.data.password;
      $rootScope.currentUser = res.data;
      localStorage.setItem('mooverUser', JSON.stringify($rootScope.currentUser));
      return $rootScope.currentUser;
    }
    return function(){
      if($('h2.form-signin-heading').length>0){
        toastr.error('כתובת או סיסמה לא נכונים');
        return;
      }
      var instance = $uibModal.open({
        templateUrl: 'templates/loginModal.html',
        controller: 'LoginModalCtrl',
        controllerAs: 'LoginModalCtrl'
      });
      return instance.result.then(assignCurrentUser);
    };
  });

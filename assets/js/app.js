'use strict';

/**
 * @ngdoc overview
 * @name mooverClientApp
 * @description
 * # mooverClientApp
 *
 * Main module of the application.
 */
angular
  .module('mooverClientApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'toastr',
    'environment',
    'ui.router',
    'ui.bootstrap'
  ])
  .config(function(envServiceProvider){
    envServiceProvider.config({
      domains: {
        development: ['localhost'],
        production: ['amiheines.com']
      },
      vars: {
        development:{
          apiUrl: '//localhost:1337'
        },
        production:{
          apiUrl: '//hummuscript.com/mooverApi'
        }
      }
    });
    envServiceProvider.check();
  })
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/empty");
    $stateProvider
      .state('mainmenu', {
        url: '/mainmenu',
        templateUrl: 'templates/mainmenu.html',
        controller: 'MainMenuCtrl',
        controllerAs: 'mainmenu',
        data: {
          requireLogin: true
        }
      })
      .state('empty', {
        url: '/empty',
        templateUrl: 'templates/empty.html',
        controller: 'EmptyCtrl',
        controllerAs: 'empty',
        data: { requireLogin: false }
      })
      .state('about', {
        url: '/about',
        templateUrl: 'templates/about.html',
        controller: 'EmptyCtrl',
        controllerAs: 'about',
        data: { requireLogin: false }
      })
      .state('contact', {
        url: '/contact',
        templateUrl: 'templates/contact.html',
        controller: 'EmptyCtrl',
        controllerAs: 'contact',
        data: { requireLogin: false }
      })
      .state('faq', {
        url: '/faq',
        templateUrl: 'templates/faq.html',
        controller: 'EmptyCtrl',
        controllerAs: 'faq',
        data: { requireLogin: false }
      })
      .state('createorder', {
        url: '/createorder',
        templateUrl: 'templates/createorder.html',
        controller: 'CreateOrderCtrl',
        controllerAs: 'createorder',
        data: {
          requireLogin: true
        }
      })
      // .state('createorder1', {
      //   url: '/createorder1',
      //   templateUrl: 'templates/createorder1.html',
      //   controller: 'CreateOrder1Ctrl',
      //   controllerAs: 'createorder1',
      //   data: {
      //     requireLogin: true
      //   }
      // })
      // .state('createorder2', {
      //   url: '/createorder2',
      //   templateUrl: 'templates/createorder2.html',
      //   controller: 'CreateOrder2Ctrl',
      //   controllerAs: 'createorder2',
      //   data: {
      //     requireLogin: true
      //   }
      // })
      // .state('createorder3', {
      //   url: '/createorder3',
      //   templateUrl: 'templates/createorder3.html',
      //   controller: 'CreateOrder3Ctrl',
      //   controllerAs: 'createorder3',
      //   data: {
      //     requireLogin: true
      //   }
      // })
      .state('orderstatus', {
        url: '/orderstatus',
        templateUrl: 'templates/orderstatus.html',
        controller: 'OrderStatusCtrl',
        controllerAs: 'orderstatus',
        data: {
          requireLogin: true
        }
      })
      .state('shippmenthistory', {
        url: '/shippmenthistory',
        templateUrl: 'templates/shippmenthistory.html',
        controller: 'ShippmentHistoryCtrl',
        controllerAs: 'shippmenthistory',
        data: {
          requireLogin: true
        }
      })
      .state('shippmenthistoryForShipper', {
        url: '/shippmenthistoryForShipper',
        templateUrl: 'templates/shippmenthistoryForShipper.html',
        controller: 'ShippmentHistoryForShipperCtrl',
        controllerAs: 'shippmenthistoryForShipper',
        data: {
          requireLogin: true
        }
      })
      .state('offers', {
        url: '/offers',
        templateUrl: 'templates/offers.html',
        controller: 'OffersCtrl',
        controllerAs: 'offers',
        data: {
          requireLogin: true
        }
      })
      .state('offersForDelivery', {
        url: '/offersForDelivery/:id',
        templateUrl: 'templates/offersForDelivery.html',
        controller: 'OffersForDeliveryCtrl',
        controllerAs: 'offersForDelivery',
        data: {
          requireLogin: true
        }
      })
      .state('offersForDeliveryContact', {
        url: '/offersForDeliveryContact/:deliveryId/:offerId',
        templateUrl: 'templates/offersForDeliveryContact.html',
        controller: 'OffersForDeliveryContactCtrl',
        controllerAs: 'offersForDeliveryContact',
        data: {
          requireLogin: true
        }
      })
      .state('offer', {
        url: '/offer/:id',
        templateUrl: 'templates/offer.html',
        controller: 'OfferCtrl',
        controllerAs: 'offer',
        data: {
          requireLogin: true
        }
      })
      .state('plannedshippments', {
        url: '/plannedshippments',
        templateUrl: 'templates/plannedshippments.html',
        controller: 'PlannedShippmentsCtrl',
        controllerAs: 'plannedshippments',
        data: {
          requireLogin: true
        }
      })
      .state('login', {
        url: '/login',
        templateUrl: 'templates/loginModal.html',
        controller: 'LoginModalCtrl',
        controllerAs: 'login',
        data: {
          requireLogin: false
        }
      })
      .state('logout', {
        url: '/logout',
        templateUrl: 'templates/logout.html',
        controller: 'LogoutCtrl',
        controllerAs: 'logout',
        data: {
          requireLogin: false
        }
      })
      .state('loginOrSignon', {
        url: '/loginOrSignon',
        templateUrl: 'templates/loginorsignon.html',
        controller: 'LoginorsignonCtrl',
        data: {
          requireLogin: false
        }
      })
      .state('/signon', {
        url: '/signon',
        templateUrl: 'templates/signon.html',
        controller: 'SignonCtrl',
        data: {
          requireLogin: false
        }
      })
      .state('/profile', {
        url: '/profile/:id',
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl',
        data: {
          requireLogin: true
        }
      })
      .state('/admin/offers', {
        url: '/admin/offers',
        templateUrl: '/admin/offers.html',
        controller: 'AdminOffersCtrl',
        data: {
          requireLogin: true
        }
      })
      .state('/admin/users', {
        url: '/admin/users',
        templateUrl: '/admin/users.html',
        controller: 'AdminUsersCtrl',
        data: {
          requireLogin: true
        }
      })
      .state('/admin/bases', {
        url: '/admin/bases',
        templateUrl: '/admin/bases.html',
        controller: 'AdminBasesCtrl',
        data: {
          requireLogin: true
        }
      })
      .state('/admin/deliveries', {
        url: '/admin/deliveries',
        templateUrl: '/admin/deliveries.html',
        controller: 'AdminDeliveriesCtrl',
        data: {
          requireLogin: true
        }
      })
      .state('/admin/delivery', {
        url: '/admin/delivery/:id',
        templateUrl: '/admin/delivery.html',
        controller: 'AdminDeliveryCtrl',
        data: {
          requireLogin: true
        }
      })
      .state('/admin/packages', {
        url: '/admin/packages',
        templateUrl: '/admin/packages.html',
        controller: 'AdminPackagesCtrl',
        data: {
          requireLogin: true
        }
      })
      ;
  })
  .config(function(toastrConfig) {
    angular.extend(toastrConfig, {
      positionClass: 'toast-top-center',
    });
  })
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($timeout, $q, $injector) {
      var loginModal, $http, $state;

      // this trick must be done so that we don't receive
      // `Uncaught Error: [$injector:cdep] Circular dependency found`
      $timeout(function () {
        loginModal = $injector.get('loginModal');
        $http = $injector.get('$http');
        $state = $injector.get('$state');
      });
      return {
        responseError: function (rejection) {
          if (rejection.status !== 401 && rejection.status !== 404) {
            return $q.reject(rejection); // rejection
          }
          var deferred = $q.defer();
          loginModal()
            .then(function () {
              deferred.resolve( $http(rejection.config) );
            })
            .catch(function () {
              $state.go('empty');
              deferred.reject(rejection);
            });
          return deferred.promise;
        }
      };
    });
  })
  .run(function($rootScope, $state, loginModal){
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
      var requireLogin = toState.data.requireLogin;
      if(!$rootScope.currentUser){
        var cachedUser = localStorage.getItem('mooverUser');
        if(cachedUser){
          $rootScope.currentUser = JSON.parse(cachedUser);
        }else{
          $rootScope.currentUser = null;
        }
      }
      if (requireLogin && !$rootScope.currentUser) {
        event.preventDefault();
        loginModal()
        .then(function () {
          return $state.go(toState.name, toParams);
        })
        .catch(function () {
          return $state.go('empty');
        });
      }
    });
  });
